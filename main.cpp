/*
CoreStuff is CoreApps activity viewer

CoreStuff is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see {http://www.gnu.org/licenses/}. */

#include "corestuff.h"

#include <QApplication>

#include <cprime/cprime.h>
#include <cprime/settingsmanage.h>

#include "button.h"


void startSetup()
{
    // set the requried folders
    CPrime::ValidityFunc::setupFileFolder( CPrime::FileFolderSetup::PinsFolder );
    CPrime::ValidityFunc::setupFileFolder( CPrime::FileFolderSetup::MimeFile );

    // if setting file not exist create one with defult
    SettingsManage *sm = SettingsManage::instance();
    sm->createDefaultSettings();
}

int main( int argc, char *argv[] )
{
	#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
		QApplication::setAttribute( Qt::AA_EnableHighDpiScaling );
	#endif

    QApplication app( argc, argv );
    app.setQuitOnLastWindowClosed(true);

    startSetup();

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName("CoreStuff");

    DockButton s;
    s.show();



    return app.exec();
}
