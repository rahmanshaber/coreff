/*
CoreBox is a simple x11 based panel and dock.

CoreBox is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see {http://www.gnu.org/licenses/}. */

#ifndef CXCBEVENTFILTER_H
#define CXCBEVENTFILTER_H

#include <QObject>
#include <QAbstractNativeEventFilter>


class X11Support;

struct Info {
    bool m_minimized;
    unsigned long windowId;
};Q_DECLARE_METATYPE(Info)

class CXCBEventFilter : public QAbstractNativeEventFilter
{

public:
    CXCBEventFilter() : m_x11support(nullptr) {}
    virtual bool nativeEventFilter(const QByteArray &eventType, void *message, long *result);
    void setX11Support(X11Support *x11support);

private:
    X11Support *m_x11support;
};

#endif // CXCBEVENTFILTER_H
