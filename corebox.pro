#-------------------------------------------------
#
# Project created by QtCreator 2018-08-20T09:12:54
#
#-------------------------------------------------

QT      += core gui widgets x11extras

TARGET   = corebox
TEMPLATE = app

VERSION  = 2.6.0

# Library section
unix:!macx: LIBS += -lcprime
LIBS    += -lX11 -lGL -lXdamage -lXcomposite -lXrender

# Disable warnings, enable threading support and c++11 
CONFIG  += thread silent build_all c++11

# Definetion section
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += "HAVE_POSIX_OPENPT"

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }

        BINDIR        = $$PREFIX/bin

        target.path   = $$BINDIR

        desktop.path  = $$PREFIX/share/applications/
        desktop.files = "corebox.desktop"

        icons.path    = /usr/share/icons/hicolor/scalable/apps/
        icons.files   = icons/corebox.svg

        INSTALLS     += target icons desktop
}

FORMS += \
    sessionsavedialog.ui \
    corestuff.ui

HEADERS += \
    button.h \
    client_c.h \
    cxcbeventfilter.h \
    sessionsavedialog.h \
    slidingstackedwidget.h \
    corestuff.h \
    tray/fixx11h.h \
    tray/lxqtglobals.h \
    tray/lxqtgridlayout.h \
    tray/lxqttray.h \
    tray/trayicon.h \
    tray/xfitman.h \
    x11support.h

SOURCES += \
    button.cpp \
    client_c.cpp \
    cxcbeventfilter.cpp \
    main.cpp \
    sessionsavedialog.cpp \
    slidingstackedwidget.cpp \
    corestuff.cpp \
    tray/lxqtgridlayout.cpp \
    tray/lxqttray.cpp \
    tray/trayicon.cpp \
    tray/xfitman.cpp \
    x11support.cpp
