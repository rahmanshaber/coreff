#ifndef BUTTON_H
#define BUTTON_H

#include <QObject>
#include <QToolButton>
#include <QMouseEvent>
#include <QShortcut>
#include <QTimer>
#include <QEvent>
#include <QHBoxLayout>

#include "corestuff.h"

#include <QPushButton>
#include <QMouseEvent>

class DockButton : public QPushButton
{
public:
    DockButton();

private:
    QPoint mousePos;
    bool isDockHide;
    void onClick();

    corestuff *cstuff = nullptr;

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

};


#endif // BUTTON_H
