/*
CoreStuff is CoreApps activity viewer

CoreStuff is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see {http://www.gnu.org/licenses/}. */

#include <QListWidgetItem>
#include <QTreeWidgetItem>
#include <QPushButton>
#include <QMenu>
#include <QFileSystemWatcher>
#include <QMessageBox>
#include <QResizeEvent>
#include <QFont>
#include <QDebug>
#include <QCollator>
#include <QMetaType>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QPainter>
#include <QScreen>

#include "slidingstackedwidget.h"
#include "sessionsavedialog.h"
#include "corestuff.h"
#include "ui_corestuff.h"

#include <cprime/cprime.h>
#include <cprime/appopenfunc.h>
#include <cprime/pinmanage.h>
#include <cprime/cplugininterface.h>

#include "x11support.h"


corestuff::corestuff(QWidget *parent):QWidget(parent),ui(new Ui::corestuff)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

	QPalette pltt( palette() );
	pltt.setColor( QPalette::Base, Qt::transparent );
	setPalette( pltt );

    loadSettings();
    startSetup();

    // Set coreapps page as active page
    on_home_clicked();

    // Load pages
    loadSystemApps();
    loadSpeedDial();
    loadSession();

    ui->menu->setVisible(0);
    ui->addSession->setVisible(0);
    ui->deleteSession->setVisible(0);
    ui->editSessoion->setVisible(0);
    ui->clearActivites->setVisible(0);

    // set window size
    QScreen *scr = QGuiApplication::primaryScreen();
    int r = scr->availableGeometry().width();
    int x = static_cast<int>(r * .35);

    ui->widgets->setFixedWidth( x - 5 );

    QTimer *timer = new QTimer( this );
    connect( timer, SIGNAL( timeout() ), this, SLOT( updateTime() ) );
    timer->start( 1000 );

    /* All Plugins */
    QStringList pluginPaths = sm->getSelectedPlugins();
    if ( not pluginPaths.count() ) {
        CPrime::InfoFunc::messageEngine(
            "Plugins not found. Please install plugins and ensure they are enabled in CoreGarage.",
            CPrime::MessageType::Info,
            this
        );
    }

    Q_FOREACH( QString pPath, pluginPaths ) {

        QPluginLoader loader(pPath);
        QObject *pObject = loader.instance();
        if (pObject) {
            plugininterface *plugin = qobject_cast<plugininterface*>(pObject);
            if ( !plugin ) {
                qWarning() << "Plugin Error:" << loader.errorString();
                continue;
            }

            QWidget *w = plugin->widget( this );
            w->setFixedWidth( x - 5 );
            ui->widgetLayout->insertWidget( 0, w );
        } else {
            qWarning() << "Plugin Error:" << loader.errorString();
        }
    }


    openWindList = new QListWidget;
    openWindList->setFlow(QListView::TopToBottom);
    openWindList->setViewMode(QListView::ListMode);

    close = new QToolButton;
    close->setText("X");
    close->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);

    // Set Settings
    openWindList->setIconSize(QSize(32,32));

    ui->pOpenedApps->addWidget(openWindList);
    ui->pOpenedApps->addWidget(close);

    // Initializing...
    m_x11support = new X11Support();
    cXCV.setX11Support(m_x11support);
    qApp->installNativeEventFilter(&cXCV);
    installEventFilter(m_x11support);

    connect(openWindList, &QListWidget::itemClicked, this, &corestuff::setFocusApp);
    connect(close,SIGNAL(clicked()),this,SLOT(closeActiveApp()));

    // Register for notifications about window property changes.
    connect(X11Support::instance(), SIGNAL(windowPropertyChanged(ulong,ulong)), this, SLOT(windowPropertyChanged(ulong,ulong)));
    connect(X11Support::instance(), SIGNAL(windowReconfigured(ulong, int, int, int, int)), this, SLOT(windowReconfigured(ulong, int, int, int, int)));

    LXQtTray *tray = new LXQtTray( this );
    connect( tray, SIGNAL( traySizeChanged( int ) ), this, SLOT( resizeDock( int ) ) );

    tray->setFixedHeight( 30 );
    tray->setIconSize( QSize( 24, 24 ) );

    ui->a->addWidget( tray );

    tray->resize( 0, 0 );
    tray->updateGeometry();

}

corestuff::~corestuff()
{
    delete ui;
}

void corestuff::updateTime()
{
    QDateTime dt = QDateTime::currentDateTime();
    ui->day->setText( dt.toString( "hh:mm AP\ndddd, MMM dd" ) );
}

void corestuff::startSetup()
{
//    ui->recentActivitesL->setIconSize(listViewIconSize);
//    ui->sessionsL->setIconSize(listViewIconSize);
//    ui->systemAppsL->setIconSize(iconViewIconSize);
//    ui->speedDialL->setIconSize(iconViewIconSize);

    // set window size
    this->setWindowState(Qt::WindowMaximized);

    connect(ui->systemAppsL,SIGNAL(itemClicked(QListWidgetItem*)),this,SLOT(openSelectedSystemApp(QListWidgetItem*)));
    connect(ui->speedDialL,SIGNAL(itemClicked(QListWidgetItem*)),this,SLOT(openSelectedSpeedDial(QListWidgetItem*)));
    connect(ui->recentActivitesL,SIGNAL(itemClicked(QTreeWidgetItem*,int)),this,SLOT(openSelectedRecentActivites(QTreeWidgetItem*,int)));
    connect(ui->sessionsL,SIGNAL(itemClicked(QTreeWidgetItem*,int)),this,SLOT(openSelectedSession(QTreeWidgetItem*,int)));


    // Configure Recent Activity
    if (sm->getShowRecent()) {
        QString raFile = CPrime::Variables::CC_CoreApps_RecentActFilePath();
        QFile file(raFile);

        if (!file.exists()) {
            // You can get error
            // Need a check here
            file.open(QIODevice::ReadWrite | QIODevice::Text);
            file.close();
        }

        loadRecent();
    }

    fswStart = new QFileSystemWatcher(this);
    connect(fswStart, &QFileSystemWatcher::fileChanged, [ = ](const QString & path) {
        reload(path);
    });

    fswStart->addPaths(QStringList() << QString(CPrime::Variables::CC_CoreApps_RecentActFilePath()));
}

QIcon corestuff::resizeIcon(QIcon originalIcon, QSize containerSize)
{
    QIcon resized = originalIcon;
    if (resized.isNull()) {
        resized = QIcon::fromTheme("application-x-executable");
    } else {
        QSize iSize = containerSize;
        QPixmap iPix = resized.pixmap(iSize);
        QSize actualSize = iPix.size();
        if ((actualSize.width() < iSize.width()) || actualSize.height() < iSize.height()) {
            QPixmap pix(iSize);
            pix.fill(Qt::transparent);

            QPainter painter(&pix);
            painter.setRenderHint(QPainter::Antialiasing);
            painter.setCompositionMode(QPainter::CompositionMode_DestinationOver);

            QRect iconRect(QPoint((iSize.width() - actualSize.width() ) / 2, (iSize.height() - actualSize.height()) / 2), actualSize);
            painter.drawPixmap(iconRect, iPix);

            resized = QIcon(pix);
        }
    }

    return resized;
}

/**
 * @brief Loads application settings
 */
void corestuff::loadSettings()
{
//    sideViewIconSize = sm->getSideViewIconSize();
//    listViewIconSize = sm->getListViewIconSize();
//    iconViewIconSize = sm->getIconViewIconSize();

    // Check is recent disabled or not
    if (sm->getShowRecent() == 0) {
        ui->recentActivites->setVisible(0);
        ui->recentActivitesL->clear();
        ui->pages->setCurrentIndex(0);
    } else {
        ui->recentActivites->setVisible(1);
        loadRecent();
    }
}


// ====================== System Apps ======================================

void corestuff::loadSystemApps()
{
    QStringList appList = sm->getApps();
    foreach (QString app, appList) {
        DesktopFile file = DesktopFile(CPrime::Variables::CC_System_AppPath() + "/" + app + ".desktop");
        QIcon icon = ApplicationDialog::searchAppIcon(file);
        icon = resizeIcon(icon, ui->systemAppsL->iconSize());
        QListWidgetItem *item = new QListWidgetItem(icon, file.getName());
        item->setData(Qt::UserRole, file.getFileName());
        item->setSizeHint(QSize(110, 100));
        ui->systemAppsL->addItem(item);
    }

    QList<DesktopFile> list = ApplicationDialog::getApplications();

    std::sort(list.begin(), list.end(), [ ](const DesktopFile& lhs, const DesktopFile& rhs) {
        return lhs.getName() < rhs.getName();
    });

    QFuture<void> f =  QtConcurrent::run([this, list, appList] {
        foreach (DesktopFile df, list) {
            if (appList.contains(df.getPureFileName())) {
                continue;
            }

            QIcon icon = ApplicationDialog::searchAppIcon( df );
            icon = resizeIcon(icon, ui->systemAppsL->iconSize());

            QListWidgetItem *item = new QListWidgetItem( icon, df.getName());
            item->setData(Qt::UserRole, df.getFileName());
            item->setSizeHint(QSize(110, 100));
            ui->systemAppsL->addItem(item);
        }
    });

    QFutureWatcher<void> *fw = new QFutureWatcher<void>();
    fw->setFuture(f);
    connect(fw, &QFutureWatcher<void>::finished, [&]() {
//        ui->appCollect->setSizeAdjustPolicy(QListWidget::AdjustToContents);
    });
}

void corestuff::openSelectedSystemApp(QListWidgetItem *item)   // open SpeedDial on CoreApps
{
    // Load desktop file for application
    DesktopFile df = DesktopFile(item->data(Qt::UserRole).toString());
    QString exec = df.getExec();
    QStringList temp = exec.split(' ');
    if (temp.count()) {
        QString app = temp.at(0);
        if (app.count()) {
            exec = exec.remove("%u", Qt::CaseInsensitive).remove("%f", Qt::CaseInsensitive);
            if (exec.at(exec.count() - 1) == ' ')
                exec = exec.remove(exec.count() - 1, 1);
            CPrime::AppOpenFunc::systemAppOpener(exec, nullptr);
        } else {
            qDebug() << "func(on_appCollect_itemDoubleClicked) : App name empty.";
            return;
        }
    } else {
        qDebug() << "func(on_appCollect_itemDoubleClicked) : Exec command empty.";
        return;
    }
}
// ================================================================


// ================== Speed Dial ==================================

void corestuff::openSelectedSpeedDial(QListWidgetItem *item)   // open SpeedDial on doubleclick
{
    pinmanage pm;
    CPrime::AppOpenFunc::appOpenEngine(pm.pinPath("Speed Dial", item->text()), this);
}

void corestuff::loadSpeedDial() // populate SpeedDial list
{
    ui->speedDialL->clear();

    pinmanage pm;
    QStringList list = pm.getPinNames("Speed Dial");
    QStringList mList;
    mList.clear();

    QStringList dateTimeList;
    dateTimeList.clear();

    foreach (QString s, list) {
        dateTimeList.append(pm.piningTime("Speed Dial", s));
    }

    CPrime::SortFunc::sortDateTime(dateTimeList);

    int count = list.count();
    int reverse = count - 1;

    for (int i = 0; i < count; i++) {
        for (int j = 0; j < count; j++) {
            QString bTime = pm.piningTime("Speed Dial", list.at(j));

            if (bTime.contains(dateTimeList.at(i))) {
                mList.insert(reverse, list.at(j));
                reverse--;
            }
        }
    }

    dateTimeList.clear();
    list.clear();

    for (int i = 0; i < mList.count(); ++i) {
        if (i == 15) {
            return;
        } else {

            QIcon icon = CPrime::ThemeFunc::getFileIcon(pm.pinPath("Speed Dial", mList.at(i)));
            icon = resizeIcon(icon, ui->speedDialL->iconSize());

            QListWidgetItem *item = new QListWidgetItem(icon, mList.at(i));
            item->setSizeHint(QSize(110, 100));
            ui->speedDialL->addItem(item);
        }
    }
}
// =================================================================


// ========== Recent activity ======================================

void corestuff::openSelectedRecentActivites(QTreeWidgetItem *item, int column)   // Open Recent activity on double click
{
    if (!item->text(column).contains("\t\t\t")) {
        return;
    }

    QStringList s = item->text(column).split("\t\t\t");

    QString appName = s.at(0);
    QString path = s.at(1);

    CPrime::AppOpenFunc::systemAppOpener(appName, path);

    // Function from utilities.cpp
    QString mess = appName + " opening " ;
    CPrime::InfoFunc::messageEngine(mess, CPrime::MessageType::Info, this);
}

void corestuff::loadRecent() // populate RecentActivity list
{
    ui->recentActivitesL->clear();

    QFuture<void> f = QtConcurrent::run([this]() {

        QSettings recentActivity(CPrime::Variables::CC_CoreApps_RecentActFilePath(), QSettings::IniFormat);
        QStringList topLevel = recentActivity.childGroups();
        topLevel = CPrime::SortFunc::sortDate(topLevel);

        foreach (QString group, topLevel) {
            QTreeWidgetItem *topTree = new QTreeWidgetItem();
            QString groupL = CPrime::InfoFunc::sentDateText(group);
            topTree->setText(0, groupL);
            topTree->setSizeHint(0, listViewIconSize);
            recentActivity.beginGroup(group);
            QStringList keys = recentActivity.childKeys();
            //CPrime::SortFunc::sortTime(keys);
            CPrime::SortFunc::sortTime(keys, CPrime::SortOrder::Descending);

            foreach (QString key, keys) {
                QTreeWidgetItem *child = new QTreeWidgetItem();
                QString value = recentActivity.value(key).toString();
                child->setText(0, value);
                child->setIcon(0, CPrime::ThemeFunc::getAppIcon(value.split("\t\t\t").at(0).toLower()));
                topTree->addChild(child);
            }

            recentActivity.endGroup();
            ui->recentActivitesL->insertTopLevelItem(0, topTree);
        }
    });

    qRegisterMetaType<QVector<int>>("QVector<int>");

    QFutureWatcher<void> *r = new QFutureWatcher<void>();
    r->setFuture(f);
    connect(r, &QFutureWatcher<void>::finished, [this]() {
        if (ui->recentActivitesL->model()->hasIndex(0, 0)) {
            (ui->recentActivitesL->setExpanded(ui->recentActivitesL->model()->index(0, 0), true));
        }
    });
}

void corestuff::on_clearActivites_clicked()
{
    QString msg = QString("Do you want to clear your recent activities?");
    QMessageBox message(QMessageBox::Question, tr("Confirmation"), msg, QMessageBox::Yes | QMessageBox::No);
    message.setWindowIcon(QIcon(":/icons/corestuff.svg"));

    int merge = message.exec();

    if (merge == QMessageBox::Cancel) {
        return;
    }

    if (merge == QMessageBox::Yes) {
        ui->recentActivitesL->clear();
        QFile(CPrime::Variables::CC_CoreApps_RecentActFilePath()).remove();
    }
}

// =========================================================================


// ========================== Sessions =====================================

void corestuff::loadSession()
{
    ui->sessionsL->clear();
    QFuture<void> f = QtConcurrent::run([this]() {
        // FIX ME
        QSettings session(CPrime::Variables::CC_CoreApps_SessionFilePath(), QSettings::IniFormat);

        QStringList nameList = session.childGroups();
        nameList = CPrime::SortFunc::sortList(nameList);

        foreach (QString name, nameList) {
            // Begin Session name group
            session.beginGroup(name);

            QTreeWidgetItem *nameTree = new QTreeWidgetItem;
            nameTree->setText(0, name);
            nameTree->setSizeHint(0, listViewIconSize);

            QStringList appNames = session.childGroups();

            foreach (QString appName, appNames) {
                // Begin date time group
                session.beginGroup(appName);

                if (session.childKeys().count() >= 1) {
                    QTreeWidgetItem *appNameT = new QTreeWidgetItem;
                    appNameT->setText(0, appName);
                    appNameT->setIcon(0, CPrime::ThemeFunc::getAppIcon(appName.toLower()));

                    QStringList keys = session.childKeys();

                    foreach (QString key, keys) {
                        QString value = session.value(key).toString();

                        if (value.count()) {
                            QTreeWidgetItem *child = new QTreeWidgetItem;
                            child->setText(0, value);
                            child->setIcon(0, value.count() ? CPrime::ThemeFunc::getAppIcon(value.toLower()) :
                                           CPrime::ThemeFunc::getAppIcon(appName.toLower()));
                            appNameT->addChild(child);
                        }
                    }

                    nameTree->addChild(appNameT);
                }

                session.endGroup();

            }

            session.endGroup();
            int index = ui->sessionsL->topLevelItemCount();
            ui->sessionsL->insertTopLevelItem(index, nameTree);
        }
    });

    qRegisterMetaType<QVector<int>>("QVector<int>");
}

void corestuff::on_addSession_clicked()
{
    sessionSaveDialog *ssd = new sessionSaveDialog(0, "");

    if (ssd->exec()) {
        loadSession();
    }
}

void corestuff::on_deleteSession_clicked()
{
    QString msg = QString("Do you want to delete the \"%1\" Session?").arg(ui->sessionsL->currentItem()->text(0));
    QMessageBox message(QMessageBox::Question, tr("Confirmation"), msg, QMessageBox::Yes | QMessageBox::No);
    message.setWindowIcon(QIcon(":/icons/corestuff.svg"));
    int merge = message.exec();

    if (merge == QMessageBox::Cancel) {
        return;
    }

    if (merge == QMessageBox::Yes) {
        if (ui->sessionsL->currentItem()) {
            // FIX ME : Add session file to variable.
            QSettings session(CPrime::Variables::CC_CoreApps_SessionFilePath(), QSettings::IniFormat);
            QStringList sessionList = session.childGroups();
            QString selected = ui->sessionsL->currentItem()->text(0);

            if (sessionList.contains(selected)) {
                session.remove(selected);
                ui->sessionsL->takeTopLevelItem(ui->sessionsL->currentIndex().row());
            }
        }

        CPrime::InfoFunc::messageEngine("Session Removed", CPrime::MessageType::Info, this);
    }
}

void corestuff::on_editSessoion_clicked()
{
    QString sessionName = ui->sessionsL->currentItem()->text(0);
    sessionSaveDialog *ssd = new sessionSaveDialog(1, sessionName);

    if (ssd->exec()) {
        loadSession();
    }

    on_menu_clicked(false);
}

void corestuff::openSelectedSession(QTreeWidgetItem *item, int column)
{
    QStringList nameList;
    QSettings session(CPrime::Variables::CC_CoreApps_SessionFilePath(), QSettings::IniFormat);
    QStringList group = session.childGroups();

    foreach (QString s, group) {
        session.beginGroup(s);
        nameList.append(s);
        session.endGroup();
    }

    QString selected = item->text(column);

    if (nameList.contains(selected)) {
        QFuture<void> future = QtConcurrent::run([&, item]() {
            for (int i = 0; i < item->childCount(); i++) {

                QTreeWidgetItem *midChildT = item->child(i);

                if (midChildT->childCount()) {
                    for (int j = 0; j < midChildT->childCount(); j++) {
                        CPrime::AppOpenFunc::systemAppOpener(midChildT->text(0), midChildT->child(j)->text(0));
                        QThread::currentThread()->msleep(1000);
                    }
                } else {
                    // Need to fix session
                    CPrime::AppOpenFunc::systemAppOpener(midChildT->text(0));
                    QThread::currentThread()->msleep(1000);
                }
            }
        });

        QFutureWatcher<void> *f = new QFutureWatcher<void>();
        f->setFuture(future);
        connect(f, &QFutureWatcher<void>::finished, this, &corestuff::showSessionMessage);
    }
}

void corestuff::showSessionMessage()
{
    CPrime::InfoFunc::messageEngine("Session restored successfully", CPrime::MessageType::Info, this);
}

void corestuff::on_sessionsL_itemSelectionChanged()
{
    bool checked = ui->menu->isChecked();

    if (!checked && currentPage != 3) {
        return;
    }

    if (ui->sessionsL->currentItem()) {
        // FIX ME
        QSettings session(CPrime::Variables::CC_CoreApps_SessionFilePath(), QSettings::IniFormat);
        QStringList sessionList = session.childGroups();

        if (sessionList.contains(ui->sessionsL->currentItem()->text(0))) {
            ui->editSessoion->setVisible(checked);
            ui->deleteSession->setVisible(checked);
            ui->addSession->setVisible(checked);
        } else {
            ui->editSessoion->setVisible(0);
            ui->deleteSession->setVisible(0);
            ui->addSession->setVisible(checked);
        }
    } else {
        ui->editSessoion->setVisible(0);
        ui->deleteSession->setVisible(0);
        ui->addSession->setVisible(checked);
    }
}

// ==================================================================================


// =============================== Apps =============================================

void corestuff::closeActiveApp()
{
    if (openWindList->currentItem()) {
        X11Support::closeWindow(openWindList->currentItem()->data(Qt::UserRole).value<Info>().windowId);
    }
}

void corestuff::updateClientList()
{
    openWindList->clear();
    QVector<unsigned long> windows = X11Support::getWindowPropertyWindowsArray(
        X11Support::rootWindow(), "_NET_CLIENT_LIST");

    // Handle new clients.
    for (int i = 0; i < windows.size(); i++) {
        if (!m_clients.contains(windows[i])) {
            // Skip our own windows.
            if (QWidget::find(windows[i]) != nullptr)
                continue;

            bool m_visible;
            QVector<unsigned long> windowTypes = X11Support::getWindowPropertyAtomsArray(windows[i], "_NET_WM_WINDOW_TYPE");
            QVector<unsigned long> windowStates = X11Support::getWindowPropertyAtomsArray(windows[i], "_NET_WM_STATE");

            // Show only regular windows in dock.
            // When no window type is set, assume it's normal window.
            m_visible = (windowTypes.size() == 0) || (windowTypes.size() == 1 && windowTypes[0] == X11Support::atom("_NET_WM_WINDOW_TYPE_NORMAL"));
            // Don't show window if requested explicitly in window states.
            if(windowStates.contains(X11Support::atom("_NET_WM_STATE_SKIP_TASKBAR")))
                m_visible = false;

            if(m_visible){
            }

            if(!m_visible){
                continue;
            }

            m_clients[windows[i]] = new ClientT(windows[i]);

            QListWidgetItem *item = new QListWidgetItem();

            Info info;
            info.windowId = windows[i];
            info.m_minimized = X11Support::getWindowMinimizedState(windows[i]);

            item->setText(m_clients[windows[i]]->name());

            item->setData(Qt::UserRole, QVariant::fromValue(info));
            item->setIcon(X11Support::getWindowIcon(windows[i]));
            openWindList->addItem(item);
        }
    }

    m_in_loop.clear();

    // Handle removed clients.
    for (;;) {
        bool clientRemoved = false;
        foreach(ClientT* client, m_clients) {

            if(m_in_loop.contains(client)){
                clientRemoved = false;
                break;
            }

            int handle = static_cast<int>(client->handle());

            delete m_clients[static_cast<unsigned long>(handle)];
            m_clients.remove(static_cast<unsigned long>(handle));
            clientRemoved = true;
            break;

            if (!windows.contains(static_cast<unsigned long>(handle))) {
                if(!m_in_loop.contains(client))
                    m_in_loop.append(client);
                delete m_clients[static_cast<unsigned long>(handle)];
                m_clients.remove(static_cast<unsigned long>(handle));
                clientRemoved = true;
                break;
            }
        }
        if (!clientRemoved) break;
    }
}

void corestuff::updateActiveWindow()
{
    unsigned long activeWindow = X11Support::getWindowPropertyWindow(
        X11Support::rootWindow(), "_NET_ACTIVE_WINDOW");
    if (activeWindow) {
        m_activeWindow = activeWindow;

//        for (int i = 0; i < openWindList->count(); i++) {
//            unsigned long widd = openWindList->item(i)->data(Qt::UserRole).value<Info>().windowId;
//            if (widd == activeWindow) {
//                openWindList->item(i)->setSelected(true);
//                //openWindList->setItemSelected(openWindList->item(i),1);
//                break;
//            } else {
//                openWindList->item(i)->setSelected(false);
//                //openWindList->setItemSelected(openWindList->item(i),0);
//            }
//        }
//    } else {
//        if (openWindList->selectedItems().count()) {
//            if (openWindList->currentItem()) {
//                openWindList->currentItem()->setSelected(false);
//            }
//        }
    }
}

void corestuff::windowPropertyChanged(unsigned long window, unsigned long atom)
{
    if (window == X11Support::rootWindow()) {
        if (atom == X11Support::atom("_NET_CLIENT_LIST"))
            updateClientList();

        if(atom == X11Support::atom("_NET_ACTIVE_WINDOW")) {
            updateActiveWindow();
//            for (int i = 0; i < openWindList->count(); i++) {
//                QListWidgetItem *item = openWindList->item(i);
//                if (item->data(Qt::UserRole).value<Info>().windowId == m_activeWindow) {
//                    item->setSelected(true);
//                    break;
//                }
//            }
        }

        return;
    }

    if (m_clients.contains(window))
        m_clients[window]->windowPropertyChanged(atom);
}

void corestuff::windowReconfigured(unsigned long window, int x, int y, int width, int height)
{
    Q_UNUSED(window)
    Q_UNUSED(x)
    Q_UNUSED(y)
    Q_UNUSED(width)
    Q_UNUSED(height)

    if (window == X11Support::rootWindow()) {
        return;
    }

    updateClientList();
}

void corestuff::setFocusApp(QListWidgetItem *item)
{
    //QListWidgetItem *item = openWindList->currentItem();
    if (item) {
        bool isMinimized = item->data(Qt::UserRole).value<Info>().m_minimized;
        unsigned long windowId = item->data(Qt::UserRole).value<Info>().windowId;
        // qDebug() << isMinimized << windowId << m_activeWindow;
        if (isMinimized) {
            X11Support::activateWindow(windowId);
            Info inf;
            inf.m_minimized = false;
            inf.windowId = windowId;
            item->setData(Qt::UserRole, QVariant::fromValue(inf));
        } else if (!isMinimized && windowId == m_activeWindow) {
            // qDebug() << "Setting to minimize";
//            X11Support::minimizeWindow(windowId);
//            Info inf;
//            inf.m_minimized = true;
//            inf.windowId = windowId;
//            item->setData(Qt::UserRole, QVariant::fromValue(inf));
        } else if (!isMinimized && windowId != m_activeWindow) {
            X11Support::activateWindow(windowId);
            Info inf;
            inf.m_minimized = false;
            inf.windowId = windowId;
            item->setData(Qt::UserRole, QVariant::fromValue(inf));
        }
    }

    if (openWindList->currentItem()) {
        close->setVisible(1);
    } else {
        close->setVisible(0);
    }

    this->hide();
}

// ==================================================================================

void corestuff::on_menu_clicked(bool checked)
{
    if (currentPage == 3) { // recent activites
        ui->addSession->setVisible(0);
        ui->deleteSession->setVisible(0);
        ui->editSessoion->setVisible(0);
        ui->clearActivites->setVisible(checked);
    }

    if (currentPage ==  4) { // session
        ui->clearActivites->setVisible(0);
        on_sessionsL_itemSelectionChanged();
    }
}

void corestuff::pageClick(QPushButton *btn, int i, QString windowTitle)
{
    // all button checked false
    for (QPushButton *b : ui->pageButtons->findChildren<QPushButton *>()) {
        b->setChecked(false);
    }

    btn->setChecked(true);
    ui->pages->slideInIdx(i);
    currentPage = i;
    this->setWindowTitle(windowTitle + " - CoreStuff");
}

void corestuff::on_speedDial_clicked()
{
    pageClick(ui->speedDial, 2, "Speed Dial");
    ui->menu->setVisible(0);
    ui->clearActivites->setVisible(0);
    ui->addSession->setVisible(0);
    ui->deleteSession->setVisible(0);
    ui->editSessoion->setVisible(0);
}

void corestuff::on_recentActivites_clicked()
{
    pageClick(ui->recentActivites, 3, "Recent Activites");
    ui->menu->setVisible(1);

    if (ui->menu->isChecked()) {
        ui->clearActivites->setVisible(1);
    } else {
        ui->clearActivites->setVisible(0);
    }

    ui->addSession->setVisible(0);
    ui->deleteSession->setVisible(0);
    ui->editSessoion->setVisible(0);
}

void corestuff::on_session_clicked()
{
    pageClick(ui->session, 4, "Sessions");
    ui->menu->setVisible(1);
    on_sessionsL_itemSelectionChanged();
    ui->clearActivites->setVisible(0);
}

void corestuff::reload(const QString &path)
{
    QFileInfo fi(path);

    if (fi.fileName() == CPrime::Variables::CC_CoreApps_RecentActFile()) {
        if (sm->getShowRecent()) {
            loadRecent();
        }
        // Hopefully this is not needed
        else {
            on_home_clicked();
        }
    } else if (fi.fileName() == CPrime::Variables::CC_CoreApps_PinsFile()) {
        loadSpeedDial();
    } else if (fi.fileName() == CPrime::Variables::CC_CoreApps_SessionFile()) {
        loadSession();
    }
}

void corestuff::on_home_clicked()
{
    pageClick(ui->home, 0, "Home");
    ui->menu->setVisible(0);
    ui->clearActivites->setVisible(0);
    ui->addSession->setVisible(0);
    ui->deleteSession->setVisible(0);
    ui->editSessoion->setVisible(0);
}

void corestuff::on_apps_clicked()
{
    pageClick(ui->apps, 1, "Apps");
    ui->menu->setVisible(0);
    ui->clearActivites->setVisible(0);
    ui->addSession->setVisible(0);
    ui->deleteSession->setVisible(0);
    ui->editSessoion->setVisible(0);
}

