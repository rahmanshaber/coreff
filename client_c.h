/*
CoreBox is a simple x11 based panel and dock.

CoreBox is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see {http://www.gnu.org/licenses/}. */

#ifndef CLIENT_C_H
#define CLIENT_C_H

#include <QObject>
#include <QIcon>
#include <QMap>


// Used for tracking connected windows (X11 clients).
// Client may have it's DockItem, but not necessary (for example, special windows are not shown in dock).
class ClientT
{

public:
    ClientT(unsigned long handle);
    ~ClientT();

    unsigned long handle() const{
        return m_handle;}

    bool isVisible(){
        return m_visible;}

    const QString& name() const{
        return m_name;}

    const QIcon& icon() const{
        return m_icon;}

    bool isUrgent() const{
        return m_isUrgent;}

    void windowPropertyChanged(unsigned long atom);

private:
    void updateVisibility();
    void updateName();
    void updateIcon();
    void updateUrgency();

    unsigned long m_handle;
    QString m_name;
    QIcon m_icon;
    bool m_isUrgent;
    bool m_visible;
};

#endif // CLIENT_C_H
