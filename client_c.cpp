/*
CoreBox is a simple x11 based panel and dock.

CoreBox is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see {http://www.gnu.org/licenses/}. */

#include <QDebug>

#include "client_c.h"
#include "x11support.h"


ClientT::ClientT(unsigned long handle)
{
    m_handle = handle;

    XSelectInput(QX11Info::display(), m_handle, PropertyChangeMask | StructureNotifyMask);

    updateVisibility();
    updateName();
    updateIcon();
    updateUrgency();
}

ClientT::~ClientT()
{
}

void ClientT::windowPropertyChanged(unsigned long atom)
{
    if(atom == X11Support::atom("_NET_WM_WINDOW_TYPE") || atom == X11Support::atom("_NET_WM_STATE"))
    {
        updateVisibility();
    }

    if(atom == X11Support::atom("_NET_WM_VISIBLE_NAME") || atom == X11Support::atom("_NET_WM_NAME") || atom == X11Support::atom("WM_NAME"))
    {
        updateName();
    }

    if(atom == X11Support::atom("_NET_WM_ICON"))
    {
        updateIcon();
    }

    if(atom == X11Support::atom("WM_HINTS"))
    {
        updateUrgency();
    }
}

void ClientT::updateVisibility()
{
    QVector<unsigned long> windowTypes = X11Support::getWindowPropertyAtomsArray(m_handle, "_NET_WM_WINDOW_TYPE");
    QVector<unsigned long> windowStates = X11Support::getWindowPropertyAtomsArray(m_handle, "_NET_WM_STATE");

//    // Show only regular windows in dock.
//    // When no window type is set, assume it's normal window.
    m_visible = (windowTypes.size() == 0) || (windowTypes.size() == 1 && windowTypes[0] == X11Support::atom("_NET_WM_WINDOW_TYPE_NORMAL"));
//    qDebug() << "Visibility 1" << m_visible;
//    // Don't show window if requested explicitly in window states.
    if(windowStates.contains(X11Support::atom("_NET_WM_STATE_SKIP_TASKBAR"))) {
        m_visible = false;
    }

//    qDebug() << "Visibility 2" << m_visible;
    if(/*m_dockItem == nullptr &&*/ m_visible)
    {
////		m_dockItem = m_dockApplet->dockItemForClient(this);
////		m_dockItem->addClient(this);
//        qDebug() << "UPDATED add >>>> " << this->name();
    }

    if(/*m_dockItem != nullptr &&*/ !m_visible)
    {
////        m_dockItem->removeClient(this);
////        m_dockItem = nullptr;
//        qDebug() << "UPDATED DELETE >>>> " << this->name();
    }
}

void ClientT::updateName()
{
    m_name = X11Support::getWindowName(m_handle);
}

void ClientT::updateIcon()
{
    m_icon = X11Support::getWindowIcon(m_handle);
}

void ClientT::updateUrgency()
{
    m_isUrgent = X11Support::getWindowUrgency(m_handle);
}
