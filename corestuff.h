/*
CoreStuff is CoreApps activity viewer

CoreStuff is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see {http://www.gnu.org/licenses/}. */

#ifndef CORESTUFF_H
#define CORESTUFF_H

#include <QWidget>

#include <cprime/applicationdialog.h>
#include <cprime/desktopfile.h>
#include <cprime/settingsmanage.h>

#include <QListWidget>
#include <QToolButton>
#include <QHBoxLayout>
#include <QCloseEvent>
#include <QFrame>

#include "client_c.h"
#include "cxcbeventfilter.h"

#include "tray/lxqttray.h"

class QListWidgetItem;
class QTreeWidgetItem;
class QPushButton;
class QMenu;
class QFileSystemWatcher;

class X11Support;

namespace Ui {
class corestuff;
}

class corestuff : public QWidget
{
    Q_OBJECT

public:
    explicit corestuff(QWidget *parent = nullptr);
    ~corestuff();

    void reload( const QString &path );


    CXCBEventFilter cXCV;

private slots:
    void openSelectedRecentActivites(QTreeWidgetItem *item, int column);
    void openSelectedSystemApp(QListWidgetItem *item );
    void openSelectedSpeedDial(QListWidgetItem *item );
    void openSelectedSession(QTreeWidgetItem *item, int column);
    void on_speedDial_clicked();
    void on_recentActivites_clicked();
    void on_session_clicked();
    void on_addSession_clicked();
    void on_deleteSession_clicked();
    void on_editSessoion_clicked();
    void on_clearActivites_clicked();
    void on_sessionsL_itemSelectionChanged();
    void on_menu_clicked(bool checked);

    void on_home_clicked();

    void on_apps_clicked();

    void windowPropertyChanged(unsigned long window, unsigned long atom);
    void windowReconfigured(unsigned long window, int x, int y, int width, int height);
    void closeActiveApp();


    void updateTime();

private:
    Ui::corestuff       *ui;
    SettingsManage *sm = SettingsManage::instance();
    QFileSystemWatcher  *fswStart;
    int                  currentPage = 0;
    QSize                sideViewIconSize, iconViewIconSize, listViewIconSize;


    QListWidget   *openWindList;
    int            position = 0, iconSize;
    QToolButton   *close;
    unsigned long  m_activeWindow;
    X11Support    *m_x11support;
    QList <ClientT*> m_in_loop;
    QMap<unsigned long, ClientT*> m_clients;

    void loadSpeedDial();
    void loadRecent();
    void loadSession();
    void pageClick( QPushButton *btn, int i, QString windowTitle );
    void loadSystemApps();
    void showSessionMessage();
    void loadSettings();
    void startSetup();

    QIcon resizeIcon(QIcon originalIcon, QSize containerSize);
    void updateActiveWindow();
    void setFocusApp(QListWidgetItem *item);
    void updateClientList();
};

#endif // CORESTUFF_H
