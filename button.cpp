#include "button.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QScreen>
#include <QProcess>

DockButton::DockButton()
{
    isDockHide = true;
    resize(50,50);
    setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    connect(this, &DockButton::clicked, this, &DockButton::onClick);
    cstuff = new corestuff();
}

void DockButton::onClick()
{
    if (isDockHide) {
        cstuff->show();
        isDockHide = false;
    } else {
        cstuff->hide();
        isDockHide = true;
    }
}

void DockButton::mousePressEvent(QMouseEvent *event)
{
    if (underMouse()) {
        mousePos = event->pos();
    }

    QPushButton::mousePressEvent(event);
}

void DockButton::mouseMoveEvent(QMouseEvent *event) {
    int mousePointx = (event->globalX() - mousePos.x());
    int mousePointy = (event->globalY() - mousePos.y());
    move(mousePointx, mousePointy);
}
