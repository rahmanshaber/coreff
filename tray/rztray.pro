TEMPLATE = app
TARGET = rztray

DEPENDPATH += .
INCLUDEPATH += .

QT += widgets x11extras

LIBS += -lX11 -lXdamage -lXrender -lXcomposite

# Input
HEADERS += lxqttray.h trayicon.h xfitman.h lxqtgridlayout.h
SOURCES += lxqttray.cpp trayicon.cpp xfitman.cpp lxqtgridlayout.cpp
